'use strict';

require('babel-register');
const webpack = require('../webpack.config.js');
const path = require('path');

webpack.module.loaders.push({
	test: /\.jsx?$/,
	loader: 'isparta',
	include: path.resolve(__dirname, '../src')
});

module.exports = function(config) {
	config.set({
		basePath: '../',
		frameworks: ['mocha', 'sinon-chai'],
		reporters: ['mocha', 'coverage'],
		coverageReporter: {
			reporters: [
				{
					type: 'text-summary'
				},
				{
					type: 'html',
					dir: 'coverage',
					subdir: '.'
				}
			]
		},

		browsers: ['Chrome'],

		files: [
			'https://code.jquery.com/jquery-3.1.1.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.0/js/foundation.js',
			'test/**/*.spec.js'
		],

		preprocessors: {
			'test/**/*.js': ['webpack','sourcemap'],
			'src/**/*.js': ['webpack','sourcemap']
		},

		webpack: webpack,
		webpackMiddleware: { noInfo: true },

		// Extend timeouts to be able to debug tests manually
		captureTimeout: 210000,
		browserDisconnectTolerance: 3,
		browserDisconnectTimeout : 210000,
		browserNoActivityTimeout : 210000,
	});
};
