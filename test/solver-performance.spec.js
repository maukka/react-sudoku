let expect = require('chai').expect;
let Solver = require('../src/js/solver/immutable-solver3');

describe('Solver performance', function() {
	this.timeout(0); // disable timeouts
	let ITERATIONS = 2;
	let testdata = [];

	before(() => {
		testdata = require('fs').readFileSync('test/sudoku.txt', 'utf-8')
			.split('\n')
			.filter(Boolean) // removes empty lines
			.map((line) => Solver.fromSudokuString(line.substr(0, 81)));
	});

	it('iterate ' + ITERATIONS + ' * 20 sudokus', () => {
		for (let i = 0; i < ITERATIONS; i++) {
			testdata.forEach((board) => {
				console.log("Running on " + Solver.toSudokuString(board));
				console.time("time");
				let solved = Solver.run(board);
				console.timeEnd("time");
				expect(Solver.isSolved(solved)).to.be.true;
			});
		}
	});
});



