import React from 'react';
import ReactDOM from 'react-dom';
// import { h, render, rerender } from 'react';
import expect from 'chai';
import App from 'index';

describe('App', () => {
	let scratch;

	before( () => {
		scratch = document.createElement('div');
		scratch.setAttribute("id", "app");
		(document.body || document.documentElement).appendChild(scratch);
	});

	beforeEach( () => {
		scratch.innerHTML = '';
	});

	after( () => {
		scratch.parentNode.removeChild(scratch);
		scratch = null;
	});


	describe('routing', () => {
		it('should render the Sudoku board', () => {
			ReactDOM.render(<App />, scratch);

			// TODO: for some reason this expect fails even if innerText has everything needed
			// debugger;
			expect(scratch.innerText).to.have.string('Sudoku');
		});
	});
});
