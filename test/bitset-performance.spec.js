const expect = require('chai').expect;
const FastBitSet = require("fastbitset");
// let Set = require('../src/js/solver/arrayset9').ArraySet9;
const Bitset9 = require('../src/js/solver/bitset9');
const readline = require('readline');
const fs = require('fs');

describe('Performance', () => {
	const MAX_ITER = 10000000;
	describe('FastBitSet performance', function() {
		this.timeout(0); // disable timeouts

		it('construction', () => {
			for (let i = 0; i < MAX_ITER; i++) {
				let as1 = new FastBitSet();
				let as2 = new FastBitSet([1,2,3,4,5,6,7,8,9]);
			}
		});
		it('adding & deleting', () => {
			let as1 = new FastBitSet();
			for (let i = 0; i < MAX_ITER; i++) {
				as1.add(6);
				as1.remove(6);
			}
		});
		it('size', () => {
			let as1 = new FastBitSet([1,2,3,4,5]);
			for (let i = 0; i < MAX_ITER; i++) {
				let size = as1.size();
			}
		});
		it('lookup', () => {
			let as1 = new FastBitSet([1,2,3,4,5]);
			for (let i = 0; i < MAX_ITER; i++) {
				as1.has(3);
			}
		});
	});
	describe('Bitset9 performance', function() {
		this.timeout(0); // disable timeouts

		it('construction', () => {
			for (let i = 0; i < MAX_ITER; i++) {
				let as1 = new Bitset9();
				let as2 = new Bitset9(0x1ff);
			}
		});
		it('cloning', () => {
			let as1 = new Bitset9();
			let as2 = new Bitset9(0x1ff);
			for (let i = 0; i < MAX_ITER; i++) {
				let clone1 = as1.clone();
				let clone2 = as2.clone();
			}
		});
		it('adding & deleting', () => {
			let as1 = new Bitset9();
			for (let i = 0; i < MAX_ITER; i++) {
				as1.add(6);
				as1.delete(6);
			}
		});
		it('size', () => {
			let as1 = new Bitset9([1,2,3,4,5]);
			for (let i = 0; i < MAX_ITER; i++) {
				as1.size;
			}
		});
		it('lookup', () => {
			let as1 = new Bitset9([1,2,3,4,5]);
			for (let i = 0; i < MAX_ITER; i++) {
				as1.has(3);
			}
		});
	});
	describe('Standard Set performance', function() {
		this.timeout(0); // disable timeouts

		it('construction', () => {
			for (let i = 0; i < MAX_ITER; i++) {
				let s1 = new Set();
				let s2 = new Set([1,2,3,4,5,6,7,8,9]);
			}
		});
		it('adding & deleting', () => {
			let s1 = new Set();
			for (let i = 0; i < MAX_ITER; i++) {
				s1.add(6);
				s1.delete(6);
			}
		});
		it('size', () => {
			let s1 = new Set([1,2,3,4,5]);
			for (let i = 0; i < MAX_ITER; i++) {
				s1.size;
			}
		});
		it('lookup', () => {
			let s1 = new Set([1,2,3,4,5]);
			for (let i = 0; i < MAX_ITER; i++) {
				s1.has(3);
			}
		});
	});
});
