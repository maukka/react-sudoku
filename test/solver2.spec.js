let expect = require('chai').expect;
let Solver = require('../src/js/solver/immutable-solver3');
let Bitset9 = require('../src/js/solver/bitset9');

describe('Solver', () => {
	const simple = Solver.fromSudokuString("207108309300629007060307050012000690500080001800090004021030780734805926980000013");
	const simple_failed = Solver.fromSudokuString("207108309340629007060307050012000690500080001800090004021030780734805926980000013");
	const simple_solution = Solver.fromSudokuString("247158369358629147169347852412573698593486271876291534621934785734815926985762413");
	const easy = Solver.fromSudokuString("030208070000109003280067001020003010700502006050806090500084067800701000040605020");
	const moderate = Solver.fromSudokuString("006130500007009060300000000490005086000000000870900014000000007030800600008054200");
	const moderate_solution = Solver.fromSudokuString("246137598157289463389546172492315786613478925875962314521693847734821659968754231");
	const hard = Solver.fromSudokuString("015000600300120000470008000002400080100000002080003400000700094000051007006000350");
	const hard_solution = Solver.fromSudokuString("215379648368124579479568123932415786154687932687293415521736894843951267796842351");
	const veryhard = Solver.fromSudokuString("100306009000070200000005040200800406070000030805003001050700000003020000400901002");
	const veryhard_step1 = Solver.fromSudokuString("100306009000070200000005040200800406070000030805003001650700000003020000400901002");
	const veryhard_solution = Solver.fromSudokuString("127346589548179263369285147231897456674512938895463721952738614713624895486951372");

	// These will take several seconds each
	const artoinkala = Solver.fromSudokuString("800000000003600000070090200050007000000045700000100030001000068008500010090000400");
	const artoinkala_solution = Solver.fromSudokuString("812753649943682175675491283154237896369845721287169534521974368438526917796318452");
	const stackoverflow = Solver.fromSudokuString("000070940070090005300005070087400100463000000000007080800700000700000028050268000");
	const hardestsudokuinworld = Solver.fromSudokuString("600008940900006100070040000200610000000000200089002000000060005000000030800001600");
	const hardestsudokuinworld_solution = Solver.fromSudokuString("625178943948326157371945862257619384463587291189432576792863415516294738834751629");
	const sudokuwiki_unsolvable_28 = Solver.fromSudokuString("600008940900006100070040000200610000000000200089002000000060005000000030800001600");
	const sudokuwiki_unsolvable_28_solution = Solver.fromSudokuString("625178943948326157371945862257619384463587291189432576792863415516294738834751629");

	const worst_case_for_bruteforce = Solver.fromSudokuString("..............3.85..1.2.......5.7.....4...1...9.......5......73..2.1........4...9");

	describe('Board handling', () => {
		it('clone and equals', () => {
			expect(Solver.equals(simple, Solver.clone(simple))).to.be.true;
			expect(Solver.equals(simple, Solver.clone(easy))).to.be.false;
		});
		it('getCellValue', () => {
			expect(Solver.getCellValue(simple, 0, 2)).to.equal(7);
			expect(Solver.getCellValue(simple, 8, 7)).to.equal(1);
			expect(Solver.getCellValue(simple, 0, 0)).to.equal(2);
		});
		it('isSolved', () => {
			expect(Solver.isSolved(sudokuwiki_unsolvable_28)).to.be.false;
			expect(Solver.isSolved(sudokuwiki_unsolvable_28_solution)).to.be.true;
		});
		it('isFailedCell', () => {
			expect(Solver.isFailedCell(simple_failed, 1, 7)).to.be.true;
			expect(Solver.isFailedCell(simple_failed, 0, 1)).to.be.false;
		});
		it('isFailed', () => {
			expect(Solver.isFailed(simple_failed)).to.be.true;
			expect(Solver.isFailed(stackoverflow)).to.be.false;
		});
		it('getCandidatesForCell', () => {
			const candidatesFor30 = new Bitset9([4]);
			let candidatesFor04 = new Bitset9([4,5]);
			expect(Solver.getCandidatesForCell(simple,3,0).bits).to.equal(candidatesFor30.bits);
			expect(Solver.getCandidatesForCell(simple,0,4).bits).to.equal(candidatesFor04.bits);
		});
		it('setCellValueAt', () => {
			const simpleModifiedAt04 = Solver.fromSudokuString("207158309300629007060307050012000690500080001800090004021030780734805926980000013");
			const simpleModifiedAt87 = Solver.fromSudokuString("207108309300629007060307050012000690500080001800090004021030780734805926980000413");
			expect(Solver.equals(Solver.setCellValueAt(simple,0,4,5), simpleModifiedAt04)).to.be.true;
			expect(Solver.equals(Solver.setCellValueAt(simple,8,6,4), simpleModifiedAt87)).to.be.true;
		});
	});

	describe('Solver', function() {
		this.timeout(0); // disable timeouts

		it('solveToChoice', () => {
			let resultBoard = Solver.solveToChoice(veryhard);
			expect(Solver.isSolved(resultBoard)).to.be.false;
			expect(Solver.equals(resultBoard, veryhard)).to.be.false;
			expect(Solver.equals(resultBoard, veryhard_solution)).to.be.false;
		});
		it('hidden singles', () => {
			const hiddensingles = Solver.fromSudokuString("2...7..38.....6.7.3...4.6....8.2.7..1.......6..7.3.4....4.8...9.6.4.....91..6...2");
			let resultBoard = Solver.hiddenSingles(moderate);
			expect(Solver.equals(resultBoard, moderate)).to.be.true;
			expect(hiddensingles.candidates[60].size).to.equal(3);
			expect(resultBoard.candidates[60].size).to.equal(1);
			expect(resultBoard.candidates[60].has(8)).to.be.true;
		});
		it('hidden singles repeat', () => {
			let simple2 = Solver.hiddenSingles(simple);
			let simple3 = Solver.hiddenSingles(simple2);
			expect(Solver.canReduceCandidates(simple3)).to.be.false;
		});
		it('intersections', () => {
			// expect(Solver.hasIntersections(simple)).to.be.true;
			// console.log("------");
			let resultBoard = Solver.intersections(hardestsudokuinworld);
			console.log("------");
			expect(Solver.hasIntersections(resultBoard)).to.be.false;
			console.log("------");
			expect(Solver.isSolved(resultBoard)).to.be.false;
			expect(Solver.equals(resultBoard, hardestsudokuinworld)).to.be.true; // equals only compares values, which should not be modified
		});
		it('run simple', () => {
			expect(Solver.equals(Solver.run(simple), simple_solution)).to.be.true;
		});
		it('run moderate', () => {
			expect(Solver.equals(Solver.run(moderate), moderate_solution)).to.be.true;
		});
		it('run stackoverflow', () => {
			let resultStr = Solver.run(stackoverflow);
			expect(Solver.isSolved(resultStr)).to.be.true;
		});
		it('run arto inkala', () => {
			expect(Solver.equals(Solver.run(artoinkala), artoinkala_solution)).to.be.true;
		});
		it('run hardestsudokuinworld', () => {
			expect(Solver.equals(Solver.run(hardestsudokuinworld), hardestsudokuinworld_solution)).to.be.true;
		});
		it('run worst_case_for_bruteforce', () => {
			expect(Solver.isSolved(Solver.run(worst_case_for_bruteforce))).to.be.true;
		});
	});
});


