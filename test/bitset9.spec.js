// This test can be run directly with Node 7, no need for babel translation
// Easier debugging in IDEA

let expect = require('chai').expect;
let Bitset9 = require('../src/js/solver/bitset9');

describe('Bitset9', () => {
	describe('ArraySet9 functionality', () => {
		it('construction', () => {
			let s1 = new Bitset9();
			expect(s1.size).to.equal(0);
			expect(s1.has(5)).to.be.false;

			let s2 = new Bitset9([2,4,6]);
			expect(s2.size).to.equal(3);
			expect(s2.has(2)).to.be.true;
			expect(s2.has(3)).to.be.false;
			expect(s2.has(4)).to.be.true;

			let s3 = new Bitset9(s2);
			expect(s3.size).to.equal(3);
			expect(s3.has(2)).to.be.true;
			expect(s3.has(3)).to.be.false;
			expect(s3.has(4)).to.be.true;
		});
		it('cloning', () => {
			let s1 = new Bitset9([2,4,6]);
			let s2 = s1.clone();
			expect(s2.has(2)).to.be.true;
			expect(s2.has(4)).to.be.true;
			expect(s2.has(3)).to.be.false;
			expect(s1.size).to.equal(s2.size);
		});
		it('adding', () => {
			let s1 = new Bitset9();
			expect(s1.add(3)).to.be.true;
			expect(s1.add(3)).to.be.false;
			expect(s1.has(3)).to.be.true;
			expect(s1.has(4)).to.be.false;

			expect(s1.add(0)).to.be.false;
			expect(s1.add(10)).to.be.false;
			expect(s1.has(0)).to.be.false;
			expect(s1.has(10)).to.be.false;
		});
		it('deleting', () => {
			let s1 = new Bitset9([9,6,5,4]);
			expect(s1.delete(1)).to.be.false;
			expect(s1.delete(5)).to.be.true;
			expect(s1.delete(5)).to.be.false;
			expect(s1.delete(6)).to.be.true;
			expect(s1.size).to.equal(2);
			expect(s1.has(9)).to.be.true;
			expect(s1.has(5)).to.be.false;
		});
		it('iteration', () => {
			let s1 = new Bitset9([8,6]);

			let iterator = s1.values();
			expect(iterator.next().done === true).to.be.false;
			expect(iterator.next().done === true).to.be.false;
			expect(iterator.next().done === true).to.be.true;

			let count = 0;
			for (let n of s1.values()) {
				count++;
				expect(n === 8 || n === 6).to.be.true;
			}
			expect(count).to.be.equal(2);
		});
	});
});
