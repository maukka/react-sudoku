# React Sudoku

Sudoku solver implemented with React.

## Running

```
npm install
npm run dev (open your browser to http://localhost:8080)
```

## Deploying

```
npm install netlify-cli -g
npm run build
netlify deploy
```

Path to deploy is "build". Netlify will install site-id in .netlify -file in current directory.

This app is currently deployed to:

http://chief-executive-officer-helen-14142.netlify.com/

and it has DNS name:

http://sudokusolver.netlify.com/

