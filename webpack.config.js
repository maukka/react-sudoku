let webpack = require('webpack');
let path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let CopyWebpackPlugin = require('copy-webpack-plugin');
let OfflinePlugin = require('offline-plugin');

let BUILD_DIR = path.resolve(__dirname, 'build');
let APP_DIR = path.resolve(__dirname, 'src');

const ENV = process.env.NODE_ENV || 'development';

let config = {
	context: APP_DIR,
	entry: ['./index'],
	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},
	resolve: {
		extensions: ['', '.js', '.jsx'],
		modulesDirectories: [
			path.resolve(__dirname, "src"),
			path.resolve(__dirname, "node_modules"),
			'node_modules'
		]
		// alias: {
		// 	index: path.resolve(__dirname, "src/index"),    // used for tests
		// }
	},
	module: {
		loaders : [
			{
				test : /\.jsx?$/,
				exclude: /node_modules/,
				loader : 'babel'
			}]
	},
	plugins: ([
		new HtmlWebpackPlugin({
			template: './index.html'
		}),
		new CopyWebpackPlugin([
			{from: './browserconfig.xml', to: './'},
			{from: './manifest.json', to: './'},
			{from: './css', to: './css'},
			{from: './img', to: './img'},
			{from: './js/vendor', to: './js/vendor'}
		])
	]).concat(ENV==='production' ? [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		}),
		new OfflinePlugin({
			relativePaths: false,
			ServiceWorker: {
				events: true
			},
			publicPath: '/'
		})
	] : []),
	devtool: ENV === 'production' ? 'source-map' : 'inline-source-map',
	node: { fs: 'empty' }
};

module.exports = config;
