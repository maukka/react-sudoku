import React from 'react';
import ReactDOM from 'react-dom';
import Solver from './js/solver/immutable-solver3';
import LoadSudokuDropdown from './components/load-sudoku-dropdown.jsx';
import PicknumbersComponent from './components/picknumbers-component.jsx';
import BoardComponent from './components/board-component.jsx';
import R from 'ramda';
require('what-input');

export default class App extends React.Component {

	static init(id) {
		let element = document.getElementById(id);
		if (element) {
			return ReactDOM.render(
				<App />,
				element
			);
		}
	}

	constructor(props) {
		super(props);
		// Operations usually carried out in componentWillMount go here

		this.state = {
			showPencilmarks: false,
			selectedNumber: 0,
			currBoardIndex: 0,
			history: [Solver.fromSudokuString(LoadSudokuDropdown.INITIAL)]
		};

		this.reset = this.reset.bind(this);
		this.solve = this.solve.bind(this);
		this.clear = this.clear.bind(this);
		this.loadNew = this.loadNew.bind(this);
		this.handleShowPencilmarksChange = this.handleShowPencilmarksChange.bind(this);
		this.handleSelectNumberChange = this.handleSelectNumberChange.bind(this);
		this.handleSetCellValue = this.handleSetCellValue.bind(this);
	}

	reset(sudokuString) {
		this.setState({
			// showPencilmarks: false,
			selectedNumber: 0,
			currBoardIndex: 0,
			history: [Solver.fromSudokuString(sudokuString)]
		});
	}

	loadNew(sudokuString) {
		this.reset(sudokuString);
	}

	getCurrBoard() {
		return this.state.history[this.state.currBoardIndex];
	}

	getFirstBoard() {
		return this.state.history[0];
	}

	solve(skipToChoice) {
		let resultBoard;
		if (skipToChoice) {
			resultBoard = Solver.solveToChoice(this.getCurrBoard());
			this.appendHistoryAt(resultBoard, this.state.currBoardIndex + 1);
		} else {
			Solver.runAsync(this.getCurrBoard(), (err, result) => {
				this.appendHistoryAt(result, this.state.currBoardIndex + 1);
			});
		}
	}

	clear() {
		this.reset(LoadSudokuDropdown.CLEAR);
	}

	handleShowPencilmarksChange(event) {
		this.setState({showPencilmarks: !this.state.showPencilmarks});
	}

	handleSelectNumberChange(num) {
		this.setState({selectedNumber: num});
	}

	// Setting cell value cuts possible forward history
	appendHistoryAt(newBoard, i) {
		if (i < this.state.history.length) {
			this.state.history.splice(i);
		}
		this.state.history.push(newBoard);
		this.setState({currBoardIndex: i});
	}

	handleSetCellValue(r, c, value) {
		let newBoard = Solver.setCellValueAt(this.getCurrBoard(),r,c,value);
		this.appendHistoryAt(newBoard, this.state.currBoardIndex + 1);
	}

	handleHistoryBack() {
		// prevent problems on double-clicks
		if (this.state.currBoardIndex <= 0)
			return;
		this.setState({
			currBoardIndex: this.state.currBoardIndex - 1
		});
	}
	handleHistoryForward() {
		// prevent problems on double-clicks
		if (this.state.currBoardIndex >= this.state.history.length - 1)
			return;
		this.setState({
			currBoardIndex: this.state.currBoardIndex + 1
		});
	}

	handleReduceButton() {
		let newBoard = Solver.reduceByLogic(this.getCurrBoard());
		this.appendHistoryAt(newBoard, this.state.currBoardIndex + 1);
	}

	isSkipButtonDisabled() {
		return Solver.isFailed(this.getCurrBoard()) || !Solver.canSkipToChoice(this.getCurrBoard());
	}
	isSolveButtonDisable() {
		return Solver.isSolved(this.getCurrBoard()) || Solver.isFailed(this.getCurrBoard());
	}
	isForwardButtonDisabled() {
		return this.state.currBoardIndex + 1 >= this.state.history.length;
	}
	isReduceButtonDisabled() {
		return !Solver.canReduceCandidates(this.getCurrBoard());
	}
	isBackButtonDisabled() {
		return this.state.currBoardIndex === 0;
	}
	render() {
		let successBox = "";
		if (Solver.isSolved(this.getCurrBoard())) {
			successBox = <div data-alert className="callout success">Solved!</div>;
		}
		let failedBox = "";
		if (Solver.isFailed(this.getCurrBoard())) {
			failedBox =	<div data-alert className="callout alert">Failed! Backtrack and try again!</div>;
		}
		let buttonClasses = "button control";
		let solveButtonClasses = buttonClasses;
		let skipButtonClasses = buttonClasses;
		let backButtonClasses = buttonClasses;
		let reduceButtonClasses = buttonClasses;
		let forwardButtonClasses = buttonClasses;
		if (this.isSkipButtonDisabled()) {
			skipButtonClasses = buttonClasses + " disabled";
		}
		if (this.isSolveButtonDisable()) {
			solveButtonClasses = buttonClasses + " disabled";
		}
		if (this.isForwardButtonDisabled()) {
			forwardButtonClasses = forwardButtonClasses + " disabled";
		}
		if (this.isReduceButtonDisabled()) {
			reduceButtonClasses = reduceButtonClasses + " disabled";
		}
		if (this.isBackButtonDisabled()) {
			backButtonClasses = backButtonClasses + " disabled";
		}
		return (
			<div>
				<div className="row">
					<div className="large-12 columns">
						<h1>Sudoku Board and Solver</h1>
					</div>
				</div>

				<div className="row hidesmall">
					<div className="small-12 columns hide-for-small">
						<div className="primary callout" data-closable="fade-out">
							<p>
								Select number in the bottom row and then click/tap desired destination location.
							</p>
							<button id="close" className="close-button" aria-label="Dismiss primary" type="button" data-close="">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>

				<div className="row">
					<div className="medium-3 columns text-center" id="controls">
						<LoadSudokuDropdown onChange={this.loadNew} currentSudokuString={Solver.toSudokuString(this.getFirstBoard())}/>
						<div className="row">
							<button className={solveButtonClasses} onClick={() => this.solve(false)} disabled={this.isSolveButtonDisable()}>Solve</button>
							<button className={buttonClasses} onClick={this.clear}>Clear</button>
							<button className={skipButtonClasses} onClick={() => this.solve(true)} disabled={this.isSkipButtonDisabled()}>
								Skip
								<span className="hidesmall"> to choice</span>
							</button>
						</div>
						<div className="row">
							<button className={backButtonClasses} onClick={() => this.handleHistoryBack()} disabled={this.isBackButtonDisabled()}>&lt; Back</button>
							<button className={reduceButtonClasses} onClick={() => this.handleReduceButton()} disabled={this.isReduceButtonDisabled()}>
								Reduce
								<span className="hidesmall"> by logic</span>
							</button>
							<button className={forwardButtonClasses} onClick={() => this.handleHistoryForward()} disabled={this.isForwardButtonDisabled()}>Fwd &gt;</button>
						</div>
					</div>

					<div className="medium-6 columns" id="board">
						<div className="row">
							<div className="small-12 columns">
								<BoardComponent currBoard={this.getCurrBoard()} selectedNumber={this.state.selectedNumber} showPencilmarks={this.state.showPencilmarks} handleSetCellValue={this.handleSetCellValue}/>
							</div>
						</div>

						<div className="row">
							<div className="small-12 columns text-center">
								<PicknumbersComponent selectedNumber={this.state.selectedNumber} handleSelectNumberChange={this.handleSelectNumberChange}/>
							</div>
						</div>
						<div className="row">
							<div className="small-12 columns text-center">
								{successBox}
							</div>
							<div className="small-12 columns text-center">
								{failedBox}
							</div>
						</div>
					</div>
					<div className="medium-3 columns">
						<label>
							<input type="checkbox" checked={this.state.showPencilmarks} onChange={this.handleShowPencilmarksChange} />
							Show pencilmarks
						</label>
						<p>
							<small>
								Copyright &copy; 2017 Marko Sirkiä
							</small>
						</p>
						<p>
							<small>
								Built using Zurb Foundation CSS framework with desktop Chrome, appearances with other browsers may vary.
							</small>
						</p>
					</div>
				</div>
				<div className="row hidesmall">
					<div className="small-12 columns text-center">
						<p>
							<small>
								{R.reverse(this.state.history).map((item, i) => {
									let reversedIndex = this.state.history.length - i - 1;
									if (this.state.currBoardIndex === reversedIndex) {
										return <span key={reversedIndex}>-> {reversedIndex + 1}: {Solver.toSudokuString(item)}<br/></span>;
									}
									return <span key={reversedIndex}>&nbsp;&nbsp;&nbsp;{reversedIndex + 1}: {Solver.toSudokuString(item)}<br/></span>;
								})}
							</small><br/>
						</p>
					</div>
				</div>
			</div>

		);
	}
}

$(document).foundation();
App.init("app");

if (process.env.NODE_ENV === 'production') {
	require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}

