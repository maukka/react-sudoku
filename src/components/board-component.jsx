import React from 'react';
import {render} from 'react-dom';
import Solver from '../js/solver/immutable-solver3';

export default class BoardComponent extends React.Component {

	constructor(props) {
		super(props);

		this.clickCell = this.clickCell.bind(this);
		this.getCellClass = this.getCellClass.bind(this);
		this.getBoard = this.getBoard.bind(this);
	}

	getSelectedNumber() {
		return this.props.selectedNumber;
	}

	isShowPencilmarks() {
		return this.props.showPencilmarks;
	}

	getBoard() {
		return this.props.currBoard;
	}

	clickCell(r, c) {
		let candidates = Solver.getCandidatesForCell(this.getBoard(),r,c);
		if (this.getSelectedNumber() > 0) {
			if (candidates.has(this.getSelectedNumber())) {
				this.props.handleSetCellValue(r, c, this.getSelectedNumber());
				return;
			}
		}
		if (candidates.size === 1) {
			let iter = candidates.values();
			const cand = iter.next().value;
			this.props.handleSetCellValue(r, c, cand);
		}
	}

	getCellClass(b, r, c) {
		let cands = Solver.getCandidatesForCell(b,r,c);
		if (!(Solver.getCellValue(b,r,c) > 0) && cands.size === 0) {
			// TODO: this.failed = true;
			return "alert";
		}
		if (this.getSelectedNumber() > 0)
			if (cands.has(this.getSelectedNumber()))
				return "candidate";
		return "";
	}

	renderPCRow(b, r, c, pr) {
		let classNames = "pencilmark " + this.getCellClass(b, r, c);
		const colNums = Solver.range(0, 3);
		const colItems = colNums.map((pc) =>
			<td key={pc.toString()} className={classNames}>
				{(Solver.getCandidatesForCell(b,r,c).has(pr * 3 + pc + 1) ? pr * 3 + pc + 1 : " ")}
			</td>
		);
		return colItems;
	}

	renderPencilmarks(b, r, c) {
		const rowNums = Solver.range(0, 3);
		const rowItems = rowNums.map((pr) =>
			<tr key={pr.toString()}>
				{this.renderPCRow(b, r, c, pr)}
			</tr>
		);
		return (
			<table className="pencilmarks">
				<tbody>
				{rowItems}
				</tbody>
			</table>
		);

	}

	renderRow(b, r) {
		const colNums = Solver.range(0, 9);
		const columnItems = colNums.map((c) => {
			let cellValue = Solver.getCellValue(b,r,c);
			let colval;
			if (cellValue > 0) {
				colval = <span>{cellValue}</span>;
			} else if (this.isShowPencilmarks()) {
				colval = this.renderPencilmarks(b, r, c);
			}
			let classNames = "cell cell-width " + this.getCellClass(b, r, c);
			return (
					<td key={c.toString()} className={classNames} onClick={() => this.clickCell(r, c)}>
						{colval}
					</td>
			);

		}
		);
		return (
			<tr key={r.toString()} className="row-height">
				{columnItems}
			</tr>
		);
	}

	render() {
		const rowNums = Solver.range(0, 9);
		const rowItems = rowNums.map((r) => {
			return this.renderRow(this.getBoard(), r);
		});
		return (
			<table className="board">
				<tbody>
				{rowItems}
				</tbody>
			</table>
		);
	}
}
