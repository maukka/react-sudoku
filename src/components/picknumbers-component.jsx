import React from 'react';
import Solver from '../js/solver/immutable-solver3';

export default class PicknumbersComponent extends React.Component {
	constructor(props) {
		super(props);
	}

	getSelectedNumber() {
		return this.props.selectedNumber;
	}

	handleSelectNumber(num) {
		if (this.props.selectedNumber === num) {
			this.props.handleSelectNumberChange(0);
		}
		else {
			this.props.handleSelectNumberChange(num);
		}
	}

	getPickNumberClass(num) {
		const defaultClasses = "button picknumber row-height cell-width";
		if (num === this.getSelectedNumber())
			return defaultClasses + " success";
		return defaultClasses;
	}

	render() {
		const nums = Solver.range(0, 9);
		const numItems = nums.map((i) => {
			const n = i + 1;
			return (
				<td key={n.toString()} className="picknumber cell">
					<button className={this.getPickNumberClass(n)} onClick={() => this.handleSelectNumber(n)}>{n}</button>
				</td>
			);
		});
		return (
			<table className="picknumbers">
				<tbody>
				<tr>
					{numItems}
				</tr>
				</tbody>
			</table>
		);

	}
}