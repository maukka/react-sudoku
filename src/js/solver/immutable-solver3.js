/*
 * Immutable solver, version 3.
 * Only external interfaces are immutable, internally data structures are mutable
 *
 * Board is stored as
 * Board {
 *  values: array(81) of Number "127834901827401982370..."
 *  candidates: array(81) of Set(9)
 * }
 */

// const inspect = require('util').inspect;
// const R = require('ramda');
const _ = require('lodash');
const Bitset9 = require('./bitset9');
const FastBitSet = require('fastbitset');

/* Private methods */

/* Index lookup tables */
const neighborMatrix = new Array(81); // Indexes of all 20 neighbors for each cell, arr[81][20]
const cellBlockMatrix = new Array(81); // arrays of row, column and block-indexes for each cell, arr[81][3][9]
const blockMatrix = new Array(3 * 9); // same as cellBlockMatrix, but without information per cell, only separate block, arr[27][9]

// Int -> Int -> Array of Int
function rangeI(from, len, increment) {
	return _.range(from, from + (len * increment), increment);
}

function initCellBlockMatrix() {
	for (let i = 0; i < 81; i++) {
		let r = Math.floor(i / 9);
		let c = i % 9;
		let blockBegin = (r - (r % 3)) * 9 + (c - (c % 3));

		let row = rangeI(r * 9, 9, 1);
		let column = rangeI(c, 9, 9);
		let block = rangeI(blockBegin, 3, 1).concat(rangeI(blockBegin + 9, 3, 1), rangeI(blockBegin + 18, 3, 1));
		cellBlockMatrix[i] = [row, column, block];
	}
}

function initBlockMatrix() {
	for (let i = 0; i < 9; i++) {
		let rowFirstCell = i * 9;
		blockMatrix[i] = cellBlockMatrix[rowFirstCell][0];
	}
	for (let i = 0; i < 9; i++) {
		let columnFirstCell = i;
		blockMatrix[9 + i] = cellBlockMatrix[columnFirstCell][1];
	}
	for (let i = 0; i < 9; i++) {
		let r = Math.floor(i / 3);
		let c = i % 3;
		let blockFirstCell = r * 27 + c * 3;
		blockMatrix[18 + i] = cellBlockMatrix[blockFirstCell][2];
	}
}

function initNeighborMatrix() {
	for (let i = 0; i < 81; i++) {
		let neigbors = new FastBitSet();
		for (let blockType = 0; blockType < 3; blockType++) {
			for (let bi = 0; bi < 9; bi++) {
				neigbors.add(cellBlockMatrix[i][blockType][bi]);
			}
		}
		neigbors.remove(i);
		neighborMatrix[i] = neigbors.array();
	}
}

// Select where to branch search - number of minimum candidates
// Board -> {row: r, col: c, candidates: candidates_for_selected_cell}
function selectChoicePoint(b) {
	const reduceFunc = (accu, value, index) => {
		let newsize = value.size;
		let oldsize = accu.candidates.size;
		if (newsize > 0 && newsize < oldsize) {
			let r = Math.floor(index / 9);
			let c = index % 9;
			if (getCellValueByIndex(b, r * 9 + c) > 0)
				return accu; // ignore cells that are set
			return {row: r, col: c, candidates: value};
		}
		return accu;
	};
	let minFound = {row: -1, col: -1, candidates: new Bitset9(0x1ff)};
	const indexes = Solver.range(0, 81);
	let allCandidates = indexes.map((i) => getCandidatesByIndex(b, i));
	minFound = allCandidates.reduce(reduceFunc, minFound);

	return minFound;
}

// Select where to branch search - number of minimum candidates
// NOTE: Returns immediately, when cell with 2 candidates is found. Must be called after singled candidates have been exhausted.
// Board -> {row: r, col: c, candidates: candidates_for_selected_cell}
function selectChoicePoint2(b) {
	let minFound = {row: -1, col: -1, candidates: new Bitset9(0x1ff)};
	for (let i = 0; i < 81; i++) {
		if (getCellValueByIndex(b, i) > 0)
			continue;
		let newcands = getCandidatesByIndex(b, i);
		let newsize = newcands.size;
		let oldsize = minFound.candidates.size;
		if (newsize > 0 && newsize < oldsize) {
			minFound = {row: Math.floor(i / 9), col: i % 9, candidates: newcands};
			if (newsize === 2)
				return minFound; // optimization, cannot find lower than 2 if used after skipToChoice()
		}
	}
	return minFound;
}

// may return 0, which means that cell value is not set
// Board -> Int -> Int
function getCellValueByIndex(b, i) {
	return b.values[i];
}

// Board -> Int -> Int
function getCandidatesByIndex(b, i) {
	return b.candidates[i];
}

// if cell value is set, return empty set of candidates. Used only in board initialization.
// Board -> Int -> Int -> Set
function calculateCandidatesForCell(values, i) {
	if (values[i] > 0) {
		return new Bitset9();
	}
	let candidates = new Bitset9(0x1ff);
	for (let n of neighborMatrix[i]) {
		let v = values[n];
		if (v > 0)
			candidates.delete(v);
	}
	return candidates;
}


// Array(81) of Number -> Array(81) of Set(9)
function calculateCandidates(values) {
	return Solver.range(0, 81).map((i) => calculateCandidatesForCell(values, i));
}

// Board -> Int -> Boolean
function isFailedCellByIndex(b, i) {
	let candidates = getCandidatesByIndex(b, i);
	let value = getCellValueByIndex(b, i);
	return value === 0 && candidates.size === 0;
}

// Board -> Int -> Int -> nil
function removeCandidateFromNeighborsMutable(b, i, newvalue) {
	for (let n of neighborMatrix[i]) {
		b.candidates[n].delete(newvalue);
	}
}

// Board -> Int -> Int -> nil
function setCellValueAtIndexMutable(b, i, newvalue) {
	// console.assert(cands.size >= 1);
	// console.assert(cands.has(newvalue));
	// console.assert(getCellValueByIndex(b, i) === 0);

	removeCandidateFromNeighborsMutable(b, i, newvalue);
	b.values[i] = newvalue;
	b.candidates[i] = new Bitset9(); // clear candidates
}

// Returns true, if b was modified, must be called again
// Board -> Boolean
function solveToChoiceMutable(b) {
	let modified = false;
	for (let i = 0; i < 81; i++) {
		let candidates = getCandidatesByIndex(b, i);
		if (getCellValueByIndex(b, i) === 0 && candidates.size === 1) {
			let candIterator = candidates.values();
			let newValue = candIterator.next().value;
			setCellValueAtIndexMutable(b, i, newValue);
			modified = true;
		}
	}
	return modified;
}

// Count number of times n exists is sets
// Optimization: bail out, if count is > 1, bigger counts not needed now
function countAppearancesOfCandidateInBlock(b, blockIndexArray, n) {
	let accu = 0;
	let where = -1;
	for (let i of blockIndexArray) {
		if (b.candidates[i].has(n)) {
			if (++accu > 1)
				return {numAppearances: accu, index: where};
			where = i;
		}
	}
	return {numAppearances: accu, index: where};
}

// Board -> nil
function removeHiddenSingleMutable(b, i, newvalue) {
	b.candidates[i] = new Bitset9([newvalue]); // removes other candidates
	removeCandidateFromNeighborsMutable(b, i, newvalue);
}

// Remove other candidates from hidden single cells
// Return true, if board was modified
// Board -> Boolean
function hiddenSinglesMutable(b) {
	let modified = false;

	for (let n = 1; n < 10; n++) {
		for (let blockType = 0; blockType < 3; blockType++) {
			for (let bi = 0; bi < 9; bi++) {
				let blockIndexes = blockMatrix[blockType * 9 + bi];
				let countResult = countAppearancesOfCandidateInBlock(b, blockIndexes, n);
				if (countResult.numAppearances === 1) {
					let candidates = getCandidatesByIndex(b, countResult.index);
					// console.assert(candidates.has(n));
					if (candidates.size > 1) {
						// console.log("hiddenSinglesMutable: modifying (" + Math.floor(countResult.index / 9) + "," + (countResult.index % 9) + ") "
						// 	+ n + " blockType " + blockType + " candidates before: " + candidates);
						removeHiddenSingleMutable(b, countResult.index, n);
						modified = true;
					}
				}
			}
		}
	}
	return modified;
}

// Find all appearances of candidate n in a block. Return Set of indices i.
// Board -> Array -> Int -> Set
function findCandidateInBlock(b, blockIndexArray, n) {
	let accu = new FastBitSet();
	for (let i of blockIndexArray) {
		if (b.values[i] === 0 && b.candidates[i].has(n)) {
			accu.add(i);
		}
	}
	return accu;
}

// Board -> Array -> Array -> Int -> Boolean
function intersectionsBlockMutable(board, line, block, n, linetype) {

	let lockedCells_size = block.intersection_size(line);
	let otherCellsInBlock = block.difference(line);
	let otherCellsInLine = line.difference(block);

	// Locked cells type 1: eliminate cells outside of a block
	if (otherCellsInBlock.size === 0 && lockedCells_size > 0) {
		if (otherCellsInLine.size > 0) {
			// console.log(linetype + " N = " + n + ": locked cells " + inspect(lockedCells) + " eliminates " + inspect(otherCellsInLine));
			for (let i of otherCellsInLine.array()) {
				getCandidatesByIndex(board, i).delete(n);
				// let deleted = getCandidatesByIndex(board, i).delete(n);
				// console.assert(deleted, "ERROR: Cannot delete n = " + n + " from i = " + 1 + inspect(getCandidatesByIndex(board, i)));
			}
			return true;
		}
	}

	// Locked cells type 2: eliminate cells within of a block locked out by row or col
	if (otherCellsInLine.size === 0 && lockedCells_size > 0) {
		if (otherCellsInBlock.size > 0) {
			// console.log(linetype + " N = " + n + ": locked cells " + inspect(lockedCells) + " eliminates " + inspect(otherCellsInBlock));
			for (let i of otherCellsInBlock.array()) {
				getCandidatesByIndex(board, i).delete(n);
				// let deleted = getCandidatesByIndex(board, i).delete(n);
				// console.assert(deleted, "ERROR: Cannot delete n = " + n + " from i = " + 1 + inspect(getCandidatesByIndex(board, i)));
			}
			return true;
		}
	}

	return false;
}

// Find and remove locked cells (hidden singles is special case of this)
// Return true, if board was modified
// Board -> Boolean
function intersectionsMutable(board) {
	let ROW_OFFSET = 9;
	let BLOCK_OFFSET = 18;

	for (let n = 1; n < 10; n++) {
		let rows = new Array(9);
		let cols = new Array(9);
		let blocks = new Array(9);


		for (let r = 0; r < 9; r++) {
			let bstart = Math.floor(r / 3) * 3;
			for (let b = bstart; b < 9; b++) {
				if (!rows[r])
					rows[r] = findCandidateInBlock(board, blockMatrix[r], n);
				if (!blocks[b])
					blocks[b] = findCandidateInBlock(board, blockMatrix[b + BLOCK_OFFSET], n);

				if (intersectionsBlockMutable(board, rows[r], blocks[b], n, "ROW"))
					return true;
			}
		}
		for (let c = 0; c < 9; c++) {
			let bstart = Math.floor(c / 3);
			for (let b = bstart; b < 9; b += 3) {
				if (!cols[c])
					cols[c] = findCandidateInBlock(board, blockMatrix[c + ROW_OFFSET], n);
				if (!blocks[b])
					blocks[b] = findCandidateInBlock(board, blockMatrix[b + BLOCK_OFFSET], n);
				if (intersectionsBlockMutable(board, cols[c], blocks[b], n, "COL"))
					return true;
			}
		}
	}
	return false;
}

/* Public methods */

class Solver {

	// String -> Board
	static fromSudokuString(str) {
		let values = [...str].map((c) => {
			if (c === '.')
				return 0;
			return c - '0';
		});
		let candidates = calculateCandidates(values);
		return {values, candidates};
	}

	// Board -> String
	static toSudokuString(b) {
		return b.values.map((n) => '0123456789'.charAt(n)).join('');
	}

	// Board -> Board
	static clone(b) {
		let newCandidates = new Array(81);
		for (let i = 0; i < 81; i++)
			newCandidates[i] = new Bitset9(b.candidates[i]);
		return {values: b.values.slice(0), candidates: newCandidates};
	}

	// Board -> Boolean
	static equals(l, r) {
		return l.values.every((v, i) => v === r.values[i]);
	}

	// from inclusive, to exclusive range(0,3) => [0,1,2]
	static range(from, to) {
		let result = new Array(to - from);
		let n = from;
		while (n < to) {
			result[n - from] = n;
			n += 1;
		}
		return result;
	}

	// may return 0, which means that cell value is not set
	// Board -> Int -> Int -> Int
	static getCellValue(b, r, c) {
		return getCellValueByIndex(b, r * 9 + c);
	}

	// Board -> Boolean
	static isSolved(b) {
		return b.values.indexOf(0) === -1;
	}

	// Board -> Int -> Int -> Boolean
	static isFailedCell(b, r, c) {
		return isFailedCellByIndex(b, r * 9 + c);
	}

	// Board -> Boolean
	static isFailed(b) {
		return Solver.range(0, 81).some((i) => isFailedCellByIndex(b, i));
	}

	// if cell value is set, return empty set of candidates
	// Board -> Int -> Int -> Set
	static getCandidatesForCell(b, row, col) {
		return getCandidatesByIndex(b, row * 9 + col);
	}

	// External immutable version - returns clone of board
	// Board -> Int -> Int -> Int -> Board
	static setCellValueAt(b, row, col, newvalue) {
		let newBoard = Solver.clone(b);
		setCellValueAtIndexMutable(newBoard, row * 9 + col, newvalue);
		return newBoard;
	}

	// Board -> Boolean
	static hasSingleCandidates(b) {
		return Solver.range(0, 81).some((i) => getCandidatesByIndex(b, i).size === 1 && getCellValueByIndex(b, i) === 0);
	}

	// Board -> Boolean
	static canReduceCandidates(b) {
		let b2 = Solver.clone(b);
		let modified = hiddenSinglesMutable(b2);
		modified = modified || intersectionsMutable(b2);
		return modified;
	}

	// Board -> Boolean
	static canSkipToChoice(b) {
		let b2 = Solver.clone(b);
		let modified = solveToChoiceMutable(b2);
		modified = modified || hiddenSinglesMutable(b2);
		modified = modified || intersectionsMutable(b2);
		return modified;
	}

	// External immutable version - returns clone of board
	// Board -> Board
	static solveToChoice(b) {
		let modified = true;
		let b2 = Solver.clone(b);
		while (modified) {
			while (modified)
				modified = solveToChoiceMutable(b2);
			modified = modified || hiddenSinglesMutable(b2);
			modified = modified || intersectionsMutable(b2);
		}
		return b2;
	}

	// NOTE: You cannot step until solution, as there is no recursion and first candidate is always
	//       selected at choice point. Step() cannot backtrack to previous choice point.
	// Board -> Board
	static step(b) {
		let cp = selectChoicePoint(b);
		if (cp.row < 0) {
			return b;
		}
		let iter = cp.candidates.values();
		let newValue = iter.next().value;
		let b2 = Solver.setCellValueAt(b, cp.row, cp.col, newValue);
		return b2;
	}

	// Board -> Board
	static hiddenSingles(b) {
		let b2 = Solver.clone(b);
		hiddenSinglesMutable(b2);
		return b2;
	}

	// Board -> Board
	static hasIntersections(b) {
		let b2 = Solver.clone(b);
		let modified = intersectionsMutable(b2);
		return modified;
	}

	// Board -> Board
	static intersections(b) {
		let modified = true;
		let b2 = Solver.clone(b);
		while (modified) {
			modified = intersectionsMutable(b2);
		}
		return b2;
	}

	// Just reduce candidates, no moves
	// Board -> Board
	static reduceByLogic(b) {
		let modified = true;
		let b2 = Solver.clone(b);
		while (modified) {
			while (modified)
				modified = hiddenSinglesMutable(b2);
			modified = modified || intersectionsMutable(b2);
		}
		return b2;
	}

	// If called without depth, then is immutable. Call internally with depth to avoid extra cloning.
	// Board -> Board
	static run(b, depth) {
		if (Solver.isSolved(b) || Solver.isFailed(b)) {
			return b;
		}
		let b2 = b;
		if (!depth) {
			b2 = Solver.clone(b);
			depth = 1;
		}
		let modified = true;
		while (modified) {
			while (modified)
				modified = solveToChoiceMutable(b2);

			// with hidden singles detection run() is 2 times faster in Node.js, but 2 times slower in Chrome
			// modified = modified || hiddenSinglesMutable(b2);

			// This intersection detection makes solving 2-3 times slower, brute-force is faster
			// modified = modified || intersectionsMutable(b2);
		}
		if (Solver.isSolved(b2) || Solver.isFailed(b2)) {
			return b2;
		}
		let cp = selectChoicePoint2(b2);
		if (cp.row < 0 || cp.candidates.size < 2) {
			// bail out, if board already messed up and failed
			return b2;
		}
		let b4;
		for (let newvalue of cp.candidates.values()) {
			let b3 = Solver.clone(b2);
			setCellValueAtIndexMutable(b3, cp.row * 9 + cp.col, newvalue);
			b4 = Solver.run(b3, depth + 1);
			if (Solver.isSolved(b4)) {
				return b4;
			}
			// when failed, discard this clone and loop another candidate
		}
		// failed, return the last failed board anyway
		return b4;
	}

	static runAsync(board, callback) {
		let b = Solver.clone(board);
		setTimeout(() => {
			let result = Solver.run(b, 0);
			if (callback) {
				callback(Solver.isFailed(result), result);
			}
		}, 0);
	}
}

initCellBlockMatrix();
initBlockMatrix();
initNeighborMatrix();
module.exports = Solver;
