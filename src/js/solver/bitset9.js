/**
 * BitSet9 - 9 bit bitset for counting Sudoku candidates
 *
 * Notes:
 *   1. new BitSet9() initializes with all 9 bits set
 *   2. set(pos) - pos is from 1 to 9
 *
 */

/*
 * Combined constructor: if parameter is
 * - undefined, initialize with all 9 bits set
 * - instance of Bitset9, copy
 * - instance of Number, set that bit only
 */
let BitSet9 = function (oldBitSet9) {
	if (oldBitSet9 !== undefined && oldBitSet9.constructor === Array) {
		this.bits = 0;
		this.size = 0;
		oldBitSet9.forEach((n) => this.add(n));
	} else if (oldBitSet9 !== undefined && oldBitSet9 instanceof BitSet9) {
		this.bits = oldBitSet9.bits;
		this.size = oldBitSet9.size;
	} else if (oldBitSet9 !== undefined && typeof oldBitSet9 === 'number') {
		this.bits = 0x1ff;
		this.size = 9;
	} else {
		this.bits = 0;
		this.size = 0;
	}
};

module.exports = BitSet9;

BitSet9.prototype = {
	clone () {
		let clone = Object.create(BitSet9.prototype);
		clone.size = this.size;
		clone.bits = this.bits;
		return clone;
	},
	add (pos) {
		// console.assert(pos >= 1 && pos <= 9, "pos must be 1..9, tried " + pos);
		if (pos < 1 || pos > 9 || this.has(pos))
			return false;
		let mask = (1 << (pos - 1));
		this.bits |= mask;
		this.size += 1;
		return true;
	},
	delete (pos) {
		// console.assert(pos >= 1 && pos <= 9, "pos must be 1..9, tried " + pos);
		if (pos < 1 || pos > 9 || !this.has(pos))
			return false;
		let mask = ~(1 << (pos - 1));
		this.bits &= mask;
		this.size -= 1;
		return true;
	},
	has (pos) {
		// console.assert(pos >= 1 && pos <= 9, "pos must be 1..9, tried " + pos);
		if (pos < 1 || pos > 9)
			return false;
		return (this.bits & (1 << (pos - 1))) > 0;
	},
	values() {
		let pos = 1;

		return {
			[Symbol.iterator]() {
				return this;
			},
			next: () => {
				for (; pos <= 9; pos++) {
					if (this.has(pos))
						break;
				}
				return pos <= 9 ?
					{value: pos++, done: false} :
					{done: true};
			}
		};
	},
	equals (rhs) {
		return this.bits === rhs.bits;
	},
	toString () {
		let result = "Bitset9 { size: " + this.size + ", [";
		for (let pos = 1; pos <= 9; pos++)
			if (this.has(pos))
				result += pos.toString() + ",";
		if (result.charAt(result.length - 1) === ',')
			result = result.substring(0, result.length - 1);
		return result  + '] }';
	}
};

